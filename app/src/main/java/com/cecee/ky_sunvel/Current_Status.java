package com.cecee.ky_sunvel;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import needle.Needle;

public class Current_Status implements Serializable {
    protected final String TAG = getClass().getSimpleName();
    Context mContext;

    private JSONArray product_jArry;



    private JSONArray ex_jArry_combi;
    // public JSONObject current_container_obj;

    public int system_total_weight, system_total_box_num;
    public String current_date_time, current_date, current_time;
    private int[] scale_raw, scale_data, ex_scale_data;
    private int[] itemViewColor, ex_itemViewColor;
    private int[] local_tare, local_i1k, local_i3k, local_i5k;
    private float[] local_f1k, local_f3k, local_f5k;
    private int combi_idx = 0;
    //public int combi_cnt = 0;
    private int[] occupy_color;
    private int[] current_parameter;



    public int[] getOccupy_color() {
        return occupy_color;
    }

    public void setOccupy_color(int[] occupy_color) {
        this.occupy_color = occupy_color;
    }

    Current_Status(Context context) {
        mContext = context;
        this.current_date = "2021-06-01 00:00";
        this.current_date = "2021-06-01";
        this.current_time = "00:00";
        this.scale_raw = new int[16];
        this.scale_data = new int[16];
        this.ex_scale_data = new int[16];
        this.local_tare = new int[16];
        this.local_i1k = new int[16];
        this.local_i3k = new int[16];
        this.local_i5k = new int[16];
        this.local_f1k = new float[16];
        this.local_f3k = new float[16];
        this.local_f5k = new float[16];
        this.ex_jArry_combi = new JSONArray();
        this.itemViewColor = new int[16];
        this.ex_itemViewColor = new int[16];
        this.occupy_color = new int[4];
        this.system_total_weight = 0;
        this.system_total_box_num = 0;
        this.current_parameter = new int[16];
    }

    public int[] getCurrent_parameter() {
        return current_parameter;
    }

    public void setCurrent_parameter(int[] current_parameter) {
        this.current_parameter = current_parameter;
    }

    public JSONArray getEx_jArry_combi() {
        return ex_jArry_combi;
    }

    public void setEx_jArry_combi(JSONArray ex_jArry_combi) {
        this.ex_jArry_combi = ex_jArry_combi;
    }

    public int[] getEx_scale_data() {
        return ex_scale_data;
    }
    public void setEx_scale_data(int[] ex_scale_data) {
        for (int i = 0; i < ex_scale_data.length; i++) this.ex_scale_data[i] = ex_scale_data[i];
        // this.ex_scale_data = ex_scale_data;
    }

    public int[] getEx_itemViewColor() {
        return ex_itemViewColor;
    }

    public void setEx_itemViewColor(int[] ex_itemViewColor) {
        this.ex_itemViewColor = ex_itemViewColor;
    }

    public int[] getItemViewColor() { return itemViewColor; }
    public void setItemViewColor(int[] itemViewColor) {
        this.itemViewColor = itemViewColor;
    }

    public float[] getLocal_f1k() {
        return local_f1k;
    }

    public void setLocal_f1k(float[] local_f1k) {
        this.local_f1k = local_f1k;
    }

    public float[] getLocal_f3k() {
        return local_f3k;
    }

    public void setLocal_f3k(float[] local_f3k) {
        this.local_f3k = local_f3k;
    }

    public float[] getLocal_f5k() {
        return local_f5k;
    }

    public void setLocal_f5k(float[] local_f5k) {
        this.local_f5k = local_f5k;
    }

    public JSONArray getProduct_jArry() {
        return product_jArry;
    }

    public void setProduct_jArry(JSONArray product_jArry) {
        this.product_jArry = product_jArry;
    }

    public String getCurrent_date_time() {
        return current_date_time;
    }

    public int[] getLocal_tare() {
        return local_tare;
    }

    public void setLocal_tare(int[] local_tare) {
        this.local_tare = local_tare;
    }

    public int[] getLocal_i1k() {
        return local_i1k;
    }

    public void setLocal_i1k(int[] local_i1k) {
        this.local_i1k = local_i1k;
    }

    public int[] getLocal_i3k() {
        return local_i3k;
    }

    public void setLocal_i3k(int[] local_i3k) {
        this.local_i3k = local_i3k;
    }

    public int[] getLocal_i5k() {
        return local_i5k;
    }

    public void setLocal_i5k(int[] local_i5k) {
        this.local_i5k = local_i5k;
    }

    public String getCurrent_time() {
        return current_time;
    }

    public void setCurrent_time(String current_time) {
        this.current_time = current_time;
    }

    public String getCurrent_date() {
        return current_date;
    }

    public void setCurrent_date(String current_date) {
        this.current_date = current_date;
    }


    public int[] getScale_raw() {
        return scale_raw;
    }

    public void setScale_raw(int[] scale_raw) {
        for (int i = 0; i < 16; i++)
            this.scale_raw[i] = scale_raw[i];
    }

    public int[] getScale_data() {
        return scale_data;
    }

    public void setScale_data(int[] scale_data) {
        for (int i = 0; i < scale_data.length; i++) this.scale_data[i] = scale_data[i];
    }

    public void funSetCurrent_date_timeViewUI() {
        Needle.onMainThread().execute(new Runnable() {
            @Override
            public void run() {
                ((MainActivity) mContext).textView_z00_date.setText(current_date_time);
                ((MainActivity) mContext).textView_z02_date.setText(current_date_time);
            }
        });
    }

    public void fun_CurrentDateTimeFromSystemTime() {
        long now = System.currentTimeMillis();
        Date date = new Date(now);
        SimpleDateFormat sdf_date_time = new SimpleDateFormat("yyyy-MM-dd a hh:mm");
        SimpleDateFormat sdf_date = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdf_time = new SimpleDateFormat("a hh:mm");
        current_date_time = sdf_date_time.format(date);
        current_date = sdf_date.format(date);
        current_time = sdf_time.format(date);
    }

    public int getIndexColor(int[] mOccupy) {
        int idx = 100;
        for (int i = 0; i < 4; i++) {
            if (mOccupy[i] == 0x00) {
                idx = i;
                break;
            }
        }
        return idx;
    }

    public JSONObject make_combination_object(int[] value) {
        JSONObject jContainer = new JSONObject();
        JSONArray mainNewJarry = new JSONArray();
        JSONArray takeOutJarry = new JSONArray();
        JSONArray jarry;
        JSONObject mjobj;
        int[] combination_item_value=new int[16];
        int tmp;
        combination_item_value = value.clone();
        int[] param = getCurrent_parameter();
        //JSONArray mjarr;
        // Log.d(TAG,"#@# @@@@@@@@@@ ex_value->"+ Arrays.toString(ex_scale_view));
        // Log.d(TAG,"#@# @@@@@@@@@@ hj_value->"+ Arrays.toString(value));
        // Log.d(TAG,"#@# @@@@@@@@@@ cv_value->"+ Arrays.toString(combination_item_value));
        // if( !ex_jArry_combi.isNull(0))
        //try {
            for (int i = 0; i < ex_jArry_combi.length(); i++) {
                int jarr_combiCnt;
                int idx;
                try {
                    mjobj = ex_jArry_combi.getJSONObject(i);
                    JSONArray _idx_Arr = mjobj.getJSONArray("idx");
                    jarr_combiCnt = _idx_Arr.length();
                    int change_cnt = 0;
                    for (int ix = 0; ix < jarr_combiCnt; ix++) {
                        idx = _idx_Arr.getInt(ix);
                        if (value[idx] < 5) change_cnt++;
                        combination_item_value[idx] = 0;//담에 조합할때 제외 시키기 위하여
                    }
                    //combi_cnt++;
                    //System.out.printf("#@# #################[%d]번째 콤비  [%d/%d]개 바뀜\n",i, change_cnt, jarr_combiCnt);
                    if (change_cnt < jarr_combiCnt) {
                        mainNewJarry.put(mjobj);//그대로 유지
                    } else//제거
                    {
                        //(1)완전히 다 빠졌음으로 하고 db에 update한다...
                        //(2) mNewJarry에 추가하지 않는다
                        // _idx=mjobj.getInt("combi_idx")*(-1);
                        //mjobj.put("combi_idx",(_idx+1)*(-1));
                        //tmp = getIndexColor(occupy_color);
                        //if(tmp<4) occupy_color[tmp]=0xaa;
                        tmp = mjobj.getInt("combi_idx");
                        occupy_color[tmp] = 0x00;
                        takeOutJarry.put(mjobj);
                        Log.d(TAG, "#@# @@@@@@@@@@ 빠져나간거~-["+tmp+"]  ->" + mjobj.toString());
                        Log.d(TAG, "#@# @@@@@@@@@@ 빠져나간거 occupy_color->" + Arrays.toString(occupy_color));
                        //완전히 빠져나간거~->{"weight":218,"shift":18,"idx":[0,2]};
                    }
                } catch (JSONException e) {
                }
            }
        //}
        //catch (NullPointerException e) {}


        jarry = combination_mix(combination_item_value, param);//새로운 조합 만듬
        if (jarry.length() > 0) {

            int lvalue_check=1;

            for (int i = 0; i < jarry.length(); i++) {
                try {
                    JSONObject _obj = jarry.getJSONObject(i);
                    JSONArray _val_arr = _obj.getJSONArray("value");
                    for(int ix=0;ix<_val_arr.length();ix++){
                        int lvalue = _val_arr.getInt(ix);
                        if(lvalue<5){
                            lvalue_check=0;
                            break;
                        }
                    }
                    Log.d(TAG,"#@# @@@@@@@@@@(0) _obj->"+_obj.toString());
                    int weight = _obj.getInt("weight");
                    if (weight > 0 && lvalue_check>0) {
                        tmp = getIndexColor(occupy_color);
                        Log.d(TAG,"#@# @@@@@@@@@@(0-1)-------- tmp["+ tmp+"] ->"+ Arrays.toString(occupy_color));
                        Log.d(TAG,"#@# @@@@@@@@@@(0) _obj->"+_obj.toString());
                        if (tmp < 4) {
                            occupy_color[tmp] = 0xAA;
                            _obj.put("combi_idx", tmp);
                            mainNewJarry.put(_obj);
                        }
                        Log.d(TAG,"#@# @@@@@@@@@@(0) mainNewJarry ->"+mainNewJarry.toString());
                        Log.d(TAG,"#@# @@@@@@@@@@(0-2)-------  tmp["+ tmp+"] ->"+ Arrays.toString(occupy_color));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        try {
            jContainer.put("take_out", takeOutJarry);
            jContainer.put("take_in", mainNewJarry);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //Log.d(TAG,"#@# @@@@@@@@@@(0) mainNewJarry["+ mainNewJarry.length()+"] ->"+mainNewJarry.toString());
        return jContainer;
    }

    public JSONArray combination_mix(int[] value, int[] param) {
        int[] combi_value = new int[value.length];//deep copy해서 사용
        int combi_min = param[0];
        int combi_max = param[1];
        int weight_min = param[2];
        int weight_max = param[3];
        int boxweight_min = param[4];
        int boxweight_max = param[5];
        JSONArray jArrCombination = new JSONArray();

        //개체중량 기준범위밖은 제외++
        for (int ix = 0; ix < value.length; ix++) {
            if (value[ix] < weight_min || value[ix] > weight_max) combi_value[ix] = 0;
            else combi_value[ix] = value[ix];
        }
        //개체중량 기준범위밖은 제외--
        for (int combi_cnt = combi_min; combi_cnt <= combi_max; combi_cnt++) {
            //Log.d(TAG,"#@# @@@@@@@@@@ combination_mix y->"+ combi_cnt);
            for (int deep = 0; deep < 5; deep++) {
                JSONObject obj = combin(combi_value, combi_cnt, boxweight_min, boxweight_max);
                if (0 == obj.length()) break;
                //Log.d(TAG,"#@# @@@@@@@@@@ combination_mix obj0->"+ obj.toString());
                try {
                    jArrCombination.put(obj);
                    JSONArray idx = obj.getJSONArray("idx");
                    //Log.d(TAG,"#@# @@@@@@@@@@ combination_mix idx->"+ idx.toString());
                    for (int i = 0; i < idx.length(); i++) {
                        int iix = idx.getInt(i);
                        combi_value[iix] = 0;
                        //Log.d(TAG,"#@# @@@@@@@@@@ idx.getInt iix->"+ iix);
                    }
                } catch (JSONException e) {
                    ;
                }
            }
        }
        //Log.d(TAG,"#@# @@@@@@@@@@ jArrCombination->"+ jArrCombination.toString());
        return jArrCombination;
    }

    public JSONObject combin(int[] in_data, int combi_min, int boxweight_min, int boxweight_max) {

        int n = in_data.length;
        int[] n_arr = new int[n];
        for (int i = 0; i < n; i++) {
            n_arr[i] = i;
        }
        int sum = 0;

        Combination comb = new Combination(n_arr.length, combi_min);
        comb.combination(n_arr, 0, 0, 0);
        ArrayList<ArrayList<Integer>> result = comb.getResult();
        List<JSONObject> jsonValues = new ArrayList<JSONObject>();

        for (int i = 0; i < result.size(); i++) {
            sum = 0;
            JSONObject object = new JSONObject();
            JSONArray idx = new JSONArray();
            JSONArray value = new JSONArray();
            //Log.d(TAG,"#@# @@@@@@@@@@@@@@@@@모든조합 경우의수 result"+ result.toString());
            try {
                boolean ok = true;
                for (int j = 0; j < result.get(i).size(); j++) {
                    int i_idx = result.get(i).get(j);
                    int item_value = in_data[i_idx];
                    if (item_value <= 0) {
                        ok = false;//묷음중 값이 한개라도 제로인거 제외
                        break;
                    }
                    sum = sum + item_value;
                    value.put(item_value);
                    idx.put(i_idx);
                }
                int shift = (sum - boxweight_min);//마이너스오차는 제외시킴
                if (ok && (sum >= boxweight_min || sum <= boxweight_max) && (shift >= 0)) {
                    //int shift = (sum > boxweight_min) ? sum - boxweight_min : 10000;//마이너스오차는 제외시킴
                    object.put("weight", sum);
                    object.put("shift", shift);
                    object.put("idx", idx);
                    object.put("value", value);
                    jsonValues.add(object);
                    //Log.d(TAG,"#@# @@@@@@@(0) List jsonValues i["+i+"] ->"+object.toString());//jsonValues->List로 저장
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        //Log.d(TAG,"#@# @@@@@@@(1) List jsonValues["+ jsonValues.size()+"] ->"+jsonValues.toString());//jsonValues->List로 저장
        JSONArray sortedJsonArray = new JSONArray();
        Collections.sort(jsonValues, new Comparator<JSONObject>() {
            private static final String KEY_NAME = "shift";

            @Override
            public int compare(JSONObject a, JSONObject b) {
                int valA = 0;// = new String();
                int valB = 0;//String valB = new String();
                try {
                    valA = a.getInt(KEY_NAME);
                    valB = b.getInt(KEY_NAME);
                } catch (JSONException e) {
                }
                int val = (valA - valB) * (1);
                return val;
            }
        });
        for (int i = 0; i < jsonValues.size(); i++) {
            sortedJsonArray.put(jsonValues.get(i));
        }
//        for (int i = 0; i < sortedJsonArray.length(); i++) {
//              Log.d(TAG,"#@# @@@@@@@@@@ "+"cnt:"+sortedJsonArray.length()+"  sortedJsonArray->"+ sortedJsonArray.toString());
//        }
        //제일 첫번째만 남김++
        JSONObject jsonResult;
        jsonResult = new JSONObject();
        try {
            if (sortedJsonArray.length() > 0) jsonResult = sortedJsonArray.getJSONObject(0);
        } catch (JSONException e) {
            ;
        }
        //제일 첫번째만 남김--
        try {
            Thread.sleep(1);
            //TimeUnit.MICROSECONDS.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return jsonResult;
    }


}
