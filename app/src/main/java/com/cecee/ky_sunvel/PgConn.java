package com.cecee.ky_sunvel;

import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by shootdol-tw on 2017-02-25.
 */
public class PgConn {
    protected final String TAG = getClass().getSimpleName();

    public static Connection openDB() {
        long mQuerytime_start, mQuerytime_end;
        Connection conn = null;
        try {
            mQuerytime_start = System.currentTimeMillis();
            Class.forName("org.postgresql.Driver");
            // "jdbc:postgresql://IP:PUERTO/DB", "USER", "PASSWORD");
            conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/postgres", "root", "898900");
            //conn = DriverManager.getConnection("jdbc:postgresql://35.194.163.158:5432/cecee", "singsky899", "898900");
        } catch (SQLException se) {
            Log.d("PgConn", "SQLException: " + se.toString());
            return null;
        } catch (ClassNotFoundException e) {
            Log.d("PgConn", "ClassNotFoundException: " + e.getMessage());
            return null;
        }
        mQuerytime_end = System.currentTimeMillis();
        Log.d("PgConn", "Conn Time: " + (mQuerytime_end - mQuerytime_start) / 1000.0);
        return conn;
    }

    public static Connection openServerDB() {
        long mQuerytime_start, mQuerytime_end;
        Connection conn = null;
        try {
            mQuerytime_start = System.currentTimeMillis();
            Class.forName("org.postgresql.Driver");
            // "jdbc:postgresql://IP:PUERTO/DB", "USER", "PASSWORD");
            conn = DriverManager.getConnection("jdbc:postgresql://35.194.163.158:5432/singsky899", "singsky899", "898900");
        } catch (SQLException se) {
            Log.d("PgConn", "SQLException: " + se.toString());
            return null;
        } catch (ClassNotFoundException e) {
            Log.d("PgConn", "ClassNotFoundException: " + e.getMessage());
            return null;
        }
        mQuerytime_end = System.currentTimeMillis();
        Log.d("PgConn", "Conn Time: " + (mQuerytime_end - mQuerytime_start) / 1000.0);
        return conn;
    }


}
