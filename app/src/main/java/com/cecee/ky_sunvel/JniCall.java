package com.cecee.ky_sunvel;

public class JniCall {
    static {
        System.loadLibrary("Jnilib");
    }
    public native  void jniOpenSerial();
    public native int[] jniGetScale();
    public native void jniSetLed(int[] led);

}
