package com.cecee.ky_sunvel;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;

import needle.Needle;
import needle.UiRelatedTask;

public class DB_Manager {
    private static final String TAG = "#@#";
    Context mContext;

    public DB_Manager(Context context) {
        mContext = context;
    }

    public void fun_setScaleReferenceFromDB() {
        Log.d(TAG, "#@# getScaleReference~~~");
        Needle.onBackgroundThread().execute(new Runnable() {
            @Override
            public void run() {
                fetchScalereference_fromDB();
            }
        });
    }

    public void fetchScalereference_fromDB() {
        int id;
        int[] itare, i1k, i3k, i5k;
        float[] f1k, f3k, f5k;
        itare = new int[32];
        i1k = new int[32];
        i3k = new int[32];
        i5k = new int[32];
        f1k = new float[32];
        f3k = new float[32];
        f5k = new float[32];

        Connection conn = PgConn.openDB();
        try {
            Statement st = conn.createStatement();
            ResultSet rs;
            rs = st.executeQuery("select * from scale_ref");
            while (rs.next()) {
                id = rs.getInt("id");
                itare[id] = rs.getInt("itare");
                i1k[id] = rs.getInt("i1k");
                i3k[id] = rs.getInt("i3k");
                i5k[id] = rs.getInt("i5k");

                f1k[id] = rs.getFloat("f1k_factor");
                f3k[id] = rs.getFloat("f3k_factor");
                f5k[id] = rs.getFloat("f5k_factor");
            }
            // Log.d(TAG,"fetchScalereference_fromDB~~~itare"+ Arrays.toString(itare));
            // Log.d(TAG,"fetchScalereference_fromDB~~~i1k"+ Arrays.toString(i1k));
            // Log.d(TAG,"fetchScalereference_fromDB~~~i3k"+ Arrays.toString(i3k));
            // Log.d(TAG,"fetchScalereference_fromDB~~~i5k"+ Arrays.toString(i5k));
            ((MainActivity) mContext).currentStatus.setLocal_tare(itare);
            ((MainActivity) mContext).currentStatus.setLocal_i1k(i1k);
            ((MainActivity) mContext).currentStatus.setLocal_i3k(i3k);
            ((MainActivity) mContext).currentStatus.setLocal_i5k(i5k);
            ((MainActivity) mContext).currentStatus.setLocal_f1k(f1k);
            ((MainActivity) mContext).currentStatus.setLocal_f3k(f3k);
            ((MainActivity) mContext).currentStatus.setLocal_f5k(f5k);

            conn.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void db_create_table() {
        db_createTable_today_product();
        db_createTable_scale_ref();
        db_createTable_items();
    }

    public void db_createTable_today_product() {
        Needle.onBackgroundThread().execute(new Runnable() {
            @Override
            public void run() {
                String query = "CREATE TABLE IF NOT exists today_product(" +
                        "stoday   VARCHAR(45) NOT NULL," +
                        "itotal_amount   INT  NULL," +
                        "itotal_box_cnt  INT  NULL," +
                        "itotal_item_cnt  INT  NULL," +
                        "faverage_box_weight   FLOAT  NULL," +
                        "faverage_box_item_cnt FLOAT  NULL," +
                        "CONSTRAINT today_product_pkey PRIMARY KEY (stoday)" +
                        ")";
                Connection conn = PgConn.openDB();
                try {
                    Statement st = conn.createStatement();
                    //ResultSet rs;
                    //rs = st.executeQuery(query);
                    int rs = st.executeUpdate(query);
                    //Log.d(TAG, rs.toString());
                    conn.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        });
    }
    public void db_createTable_items() {
        Needle.onBackgroundThread().execute(new Runnable() {
            @Override
            public void run() {
                String query = "CREATE TABLE IF NOT exists all_items(" +
                        "sname        VARCHAR(45)    PRIMARY KEY NOT NULL," +
                        "icombi_min   INT            NULL," +
                        "icombi_max   INT            NULL," +
                        "iweight_min  INT            NULL," +
                        "iweight_max  INT            NULL," +
                        "ibox_min     INT            NULL,"+
                        "ibox_max     INT            NULL,"+
                        "itime        timestamp      NULL "+
                        ");";
                Connection conn = PgConn.openDB();
                try {
                    Statement st = conn.createStatement();
                    //ResultSet rs;
                    //rs = st.executeQuery(query);
                    int rs = st.executeUpdate(query);
                    //Log.d(TAG, rs.toString());
                    conn.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        });
    }
    public void db_createTable_scale_ref() {
        Needle.onBackgroundThread().execute(new Runnable() {
            @Override
            public void run() {
                String query = "CREATE TABLE IF NOT exists scale_ref(" +
                        "id INT NOT NULL," +
                        "itare INT NULL," +
                        "i1k INT NULL," +
                        "i3k INT NULL," +
                        "i5k INT NULL," +
                        "f1k_factor FLOAT NULL," +
                        "f3k_factor FLOAT NULL," +
                        "f5k_factor FLOAT NULL," +
                        "CONSTRAINT scale_ref_pkey PRIMARY KEY (id)" +
                        ");";
                Connection conn = PgConn.openDB();
                try {
                    Statement st = conn.createStatement();
                    //ResultSet rs;
                    //rs = st.executeQuery(query);
                    int rs = st.executeUpdate(query);
                    conn.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        });
    }

    public void dbUpdate_today_product(JSONObject mjobj) {
        Needle.onBackgroundThread().execute(new Runnable() {
            //완전히 빠져나간거~->{"weight":218,"shift":18,"idx":[0,2]};
            int mweight = 0, itotal_amount = 0;
            int mitem_cnt = 0, itotal_item_cnt = 0;
            int itotal_box_cnt = 1;
            float faverage_box_weight = 0.0f;
            float faverage_box_item_cnt = 0.0f;
            String stoday = ((MainActivity) mContext).currentStatus.getCurrent_date();
            {
                Log.d(TAG, "완전히 빠져나간가서 DB업데이트~->" + mjobj.toString());
                try {
                    itotal_amount=mweight = mjobj.getInt("weight");
                    JSONArray idx_arry = mjobj.getJSONArray("idx");
                    mitem_cnt = itotal_item_cnt = idx_arry.length();
                    faverage_box_weight = (float) mweight;
                    faverage_box_item_cnt = (float)mitem_cnt;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void run() {
                String query;
                query = "SELECT * FROM today_product WHERE stoday >= '%s' AND stoday <= '%s' ORDER BY stoday LIMIT 1 ";
                query = String.format(query, stoday, stoday);
                Log.d(TAG, "query->" + query);
                Connection conn = PgConn.openDB();
                try {
                    Statement st = conn.createStatement();
                    ResultSet rs;
                    rs = st.executeQuery(query);
                    // Log.d(TAG, "wasNull rs->" + rs.);
                    while (rs.next()) {
                        itotal_amount = rs.getInt("itotal_amount") + mweight;
                        itotal_box_cnt = rs.getInt("itotal_box_cnt") + 1;
                        itotal_item_cnt = rs.getInt("itotal_item_cnt") + mitem_cnt;
                        faverage_box_weight = (float) itotal_amount / itotal_box_cnt;
                        faverage_box_item_cnt = (float) itotal_item_cnt / itotal_box_cnt;
                    }
                    System.out.printf("#@# 만들DB 데이타 stoday[%s] itotal_amount[%d] itotal_box_cnt[%d] itotal_item_cnt[%d] faverage_box_weight[%f] faverage_box_item_cnt[%f]\n",
                            stoday, itotal_amount, itotal_box_cnt, itotal_item_cnt,faverage_box_weight, faverage_box_item_cnt );
                    conn.close();
                    dbUpsert_product(stoday, itotal_amount, itotal_box_cnt, itotal_item_cnt, faverage_box_weight, faverage_box_item_cnt);

                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }//end run
        });
    }

    public JSONArray dbGetCurrentProductForRecycler(String str_from, String str_to, boolean all) {
        JSONArray oParamArry = new JSONArray();
        String query;
        if (all) {
            query = "SELECT * FROM today_product ORDER BY stoday DESC ";
        } else {
            query = "SELECT * FROM today_product WHERE stoday >= '%s' AND stoday <= '%s' ORDER BY stoday DESC ";
            query = String.format(query, str_from, str_to);
        }
        Log.d(TAG, "query->" + query);

        Connection conn = PgConn.openDB();
        try {
            Statement st = conn.createStatement();
            ResultSet rs;
            rs = st.executeQuery(query);
            while (rs.next()) {
                try {
                    JSONObject jObj = new JSONObject();
                    jObj.put("stoday", rs.getString("stoday"));
                    jObj.put("itotal_amount", rs.getInt("itotal_amount"));
                    jObj.put("itotal_box_cnt", rs.getInt("itotal_box_cnt"));
                    jObj.put("itotal_item_cnt", rs.getInt("itotal_item_cnt"));
                    jObj.put("faverage_box_weight", rs.getFloat("faverage_box_weight"));
                    jObj.put("faverage_box_item_cnt", rs.getFloat("faverage_box_item_cnt"));
                    oParamArry.put(jObj);
                } catch (JSONException e) {

                }
            }
            Log.d(TAG, "oParamArry.length--->" + oParamArry.length());
            conn.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return oParamArry;
    }

    public JSONArray fun_GetInitProductDataFromDB(String str_from, String str_to, boolean all) {
        JSONArray oParamArry = new JSONArray();
        //str_from="2021-06-01";
        //str_to="2021-06-05";
        // select * from today_product where stoday>= '2021-06-03' and stoday <='2021-06-03' order by stoday;
        Needle.onBackgroundThread().execute(new Runnable() {
            @Override
            public void run() {
                String query;
                if (all) {
                    query = "SELECT * FROM today_product ORDER BY stoday DESC ";
                } else {
                    query = "SELECT * FROM today_product WHERE stoday >= '%s' AND stoday <= '%s' ORDER BY stoday DESC ";
                    query = String.format(query, str_from, str_to);
                }

                Log.d(TAG, "query->" + query);

                Connection conn = PgConn.openDB();
                try {
                    Statement st = conn.createStatement();
                    ResultSet rs;
                    rs = st.executeQuery(query);
                    while (rs.next()) {
                        try {
                            JSONObject jObj = new JSONObject();
                            jObj.put("stoday", rs.getString("stoday"));
                            jObj.put("itotal_amount", rs.getInt("itotal_amount"));
                            jObj.put("itotal_box_cnt", rs.getInt("itotal_box_cnt"));
                            jObj.put("itotal_item_cnt", rs.getInt("itotal_item_cnt"));
                            jObj.put("faverage_box_weight", rs.getFloat("faverage_box_weight"));
                            jObj.put("faverage_box_item_cnt", rs.getFloat("faverage_box_item_cnt"));
                            //Log.d(TAG, "ts--->"+rs.getTimestamp("ts"));
                            oParamArry.put(jObj);
                        } catch (JSONException e) {

                        }
                    }
                    Log.d(TAG, "oParamArry.length--->" + oParamArry.length());
                    conn.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        });
        return oParamArry;
    }


    public void dbUpsert_product(String stoday, int itotal_amount, int itotal_box_cnt, int itotal_item_cnt, float faverage_box_weight, float faverage_box_item_cnt) {
        final String _query =
                "INSERT INTO today_product(stoday, itotal_amount, itotal_box_cnt, itotal_item_cnt, faverage_box_weight, faverage_box_item_cnt)" +
                        "VALUES('%s', %d, %d, %d, %f, %f)" +
                        "ON CONFLICT (stoday)" +
                        "DO update SET stoday='%s', itotal_amount=%d, itotal_box_cnt=%d, itotal_item_cnt=%d, faverage_box_weight=%f, faverage_box_item_cnt=%f;";

        Needle.onBackgroundThread().execute(new Runnable() {
            @Override
            public void run() {
                String query = String.format(_query, stoday, itotal_amount, itotal_box_cnt, itotal_item_cnt, faverage_box_weight, faverage_box_item_cnt,
                        stoday, itotal_amount, itotal_box_cnt, itotal_item_cnt, faverage_box_weight, faverage_box_item_cnt);

                Log.d(TAG, "dbUpsert_product query--->" + query);
                Connection conn = PgConn.openDB();
                try {
                    Statement st = conn.createStatement();
                    //rs = st.executeQuery(query);
                    int rs = st.executeUpdate(query);
                    conn.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        });
    }


    public void dbUpdateZeroAdjust() {
        int i;
        int[] m_tare=new int[16];
        int[] mScale= new int[64] ;//= ((MainActivity) mContext).jni.jniGetScale();
        for( i=0;i<5;i++){
            mScale = ((MainActivity) mContext).jni.jniGetScale();
            if(mScale[16]>0 || (mScale[0]==0 && mScale[1]==0)) {
             try {
                    Thread.sleep(200);
                } catch (Exception e) {e.printStackTrace();}
                continue;
            }//error이면
            else break;
        }
        if(i>=4) return;
        for( i=0;i<16;i++){
            m_tare[i]=mScale[i];
        }
        //int[] scale_raw = ((MainActivity) mContext).currentStatus.getScale_raw();
        //m_tare=scale_raw.clone();
        ((MainActivity) mContext).currentStatus.setLocal_tare(m_tare);
        Log.d(TAG,"#@# @@@@@@@@@@(0) dbUpdateZeroAdjust["+ m_tare+"] ->"+Arrays.toString(m_tare));
        Toast toast = Toast.makeText(((MainActivity) mContext).getApplicationContext(), "0점 조정 하였습니다.", Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP | Gravity.LEFT, 600, 100);
        toast.show();

        Needle.onBackgroundThread().execute(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < m_tare.length; i++)
                    ((MainActivity) mContext).scaleRef.REFDB_upsertTare(i, m_tare[i]);
            }
        });

    }

    public void dbUpsert_1KFactor() {
        float[] scale_factor = new float[16];
        int[] oneK=new int[16];
        //int[] scale_raw = ((MainActivity) mContext).currentStatus.getScale_raw();
        int[] scale_tare = ((MainActivity) mContext).currentStatus.getLocal_tare();

        int i;

        int[] mScale= new int[64] ;//= ((MainActivity) mContext).jni.jniGetScale();
        for( i=0;i<5;i++){
            mScale = ((MainActivity) mContext).jni.jniGetScale();
            if(mScale[16]>0 || (mScale[0]==0 && mScale[1]==0)) {
                try {
                    Thread.sleep(200);
                } catch (Exception e) {e.printStackTrace();}
                continue;
            }//error이면
            else break;
        }
        if(i>=4) return;
        for( i=0;i<16;i++){
            oneK[i]=mScale[i];
        }
       ((MainActivity) mContext).currentStatus.setLocal_i1k(oneK);
         //int[]local_tare = ((MainActivity) mContext).currentStatus.getScale_raw();
        Log.d(TAG, "dbUpsert_1KFactor scale_raw--->" + Arrays.toString(oneK));
        Log.d(TAG, "dbUpsert_1KFactor scale_tare--->" + Arrays.toString(scale_tare));

        for (i = 0; i < 16; i++) {
            scale_factor[i] = 0.0f;
            if(oneK[i]<=scale_tare[i]) scale_factor[i] = 0.0f;
            else{
                //1k변화 일때 약 40만 차이 나서 20만 정도 차이 안나면 불랼으로 취급하자!!!!
                if((oneK[i] - scale_tare[i])<200000) scale_factor[i] = 0.0f;
                else{
                    if (oneK[i] != 0) scale_factor[i] = 1000.0f / (oneK[i] - scale_tare[i]);
                    if (scale_factor[i] < 0) scale_factor[i] = 0.0f;
                }
            }
        }
        ((MainActivity) mContext).currentStatus.setLocal_f1k(scale_factor);
        String[] str_array=new String[16];
        String str="1000그램 조정 하였습니다.\r\n";
        for (i = 0; i < 14; i++) {
            str_array[i]= (scale_factor[i]==0)? String.format("[%d] 오류\r\n",i+1):String.format("[%d]  [%f] OK!!\r\n",i+1, scale_factor[i]);
            str=str+str_array[i];
        }
        Toast toast = Toast.makeText(((MainActivity) mContext).getApplicationContext(), str, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP | Gravity.LEFT, 600, 100);
        toast.show();
        final String _query =
                "INSERT INTO scale_ref (id, i1k, f1k_factor)" +
                        "VALUES(%d, %d, %f)" +
                        "ON CONFLICT (id)" +
                        "DO update SET i1k=%d, f1k_factor=%f;";
        Needle.onBackgroundThread().execute(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 16; i++) {
                    float i1k_factor = scale_factor[i];
                    int i1k = oneK[i];
                    String query = String.format(_query, i, i1k, i1k_factor, i1k, i1k_factor);
                    Connection conn = PgConn.openDB();
                    try {
                        Statement st = conn.createStatement();
                        st.executeUpdate(query);
                        conn.close();
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                }
            }
        });
    }


    public void spinnerDBAddAndView(String str_item, int[] data) {
        Needle.onBackgroundThread().execute(new UiRelatedTask<JSONArray>() {
            @Override
            protected JSONArray doWork() {

                ((MainActivity) mContext).scaleRef.REFDB_upsertToAllItemsBysname(str_item, data);//update new item
                JSONArray jArr = ((MainActivity) mContext).scaleRef.REFDB_selectAllItems(); //get all items
                Log.d("#@#","spinnerDBAddAndView"+ jArr.toString());
                return jArr;
            }

            @Override
            protected void thenDoUiRelatedWork(JSONArray jArr) {
                ((MainActivity) mContext).spinnerViewFromObject(jArr);
            }
        });
    }

    public void spinnerRemoveDBAndView(String str_item) {
        Needle.onBackgroundThread().execute(new UiRelatedTask<JSONArray>() {
            @Override
            protected JSONArray doWork() {

                ((MainActivity) mContext).scaleRef.REFDB_deleteToAllItemsBysname(str_item);//update new item
                JSONArray jArr = ((MainActivity) mContext).scaleRef.REFDB_selectAllItems(); //get all items
                Log.d("#@#","spinnerRemoveDBAndView->" +jArr.toString());
                return jArr;
            }

            @Override
            protected void thenDoUiRelatedWork(JSONArray jArr) {
                ((MainActivity) mContext).spinnerViewFromObject(jArr);
            }
        });
    }

    public void funTitleUpdateAndView(String str_item, int[] data) {
        Needle.onBackgroundThread().execute(new UiRelatedTask<JSONArray>() {
            @Override
            protected JSONArray doWork() {
                ((MainActivity) mContext).scaleRef.REFDB_upsertToAllItemsBysname(str_item, data);//update new item
                JSONArray jArr = ((MainActivity) mContext).scaleRef.REFDB_selectAllItems(); //get all items
               // Log.d("#@#", "XXXXXXXXXXXXXXXX(1)  funTitleUpdateAndView XXXXXXXXXXXXXX"+jArr.toString());
                return jArr;
            }

            @Override
            protected void thenDoUiRelatedWork(JSONArray jArr) {
                Log.d("#@#", "XXXXXXXXXXXXXXXX(2)  funTitleUpdateAndView XXXXXXXXXXXXXX"+jArr.toString());
                funTitleView_DoUI(jArr);
                ((MainActivity) mContext).spinnerViewFromObject(jArr);
            }
        });
    }

    public void funTitleView_DoUI(JSONArray jArr) {
        int tmp0,tmp1;
        String str;
        try {
            JSONObject jobj= jArr.getJSONObject(0);
            str= jobj.getString("sname");
            ((MainActivity) mContext).button_item_title.setText(str);
            tmp0=jobj.getInt("icombi_min");
            tmp1=jobj.getInt("icombi_max");
            str=String.format("%d - %d 개", tmp0, tmp1);
            ((MainActivity) mContext).textView_z00_combi_cnt.setText(str);

            tmp0=jobj.getInt("iweight_min");
            tmp1=jobj.getInt("iweight_max");
            str=String.format("%d - %d g", tmp0, tmp1);
            ((MainActivity) mContext).textView_z00_combi_weight.setText(str);

            tmp0=jobj.getInt("ibox_min");
            tmp1=jobj.getInt("ibox_max");
            str=String.format("%.1f - %.1f Kg", tmp0/1000.0f, tmp1/1000.0f);
            ((MainActivity) mContext).textView_z00_weight_box.setText(str);
         } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void funTitleViewInitFromDB() {
        Needle.onBackgroundThread().execute(new UiRelatedTask<JSONArray>() {
            @Override
            protected JSONArray doWork() {
                JSONArray jArr = ((MainActivity) mContext).scaleRef.REFDB_selectAllItems(); //get all items
                return jArr;
            }
            @Override
            protected void thenDoUiRelatedWork(JSONArray jArr) {
                funTitleView_DoUI(jArr);
            }
        });
    }

    public void spinnerViewFromDB() {
        Needle.onBackgroundThread().execute(new UiRelatedTask<JSONArray>() {
            @Override
            protected JSONArray doWork() {
                JSONArray jArr = ((MainActivity) mContext).scaleRef.REFDB_selectAllItems(); //get all items
                Log.d(TAG,"#@# @@@@@@@@@@ spinnerViewFromDB jArr ->"+jArr.toString());
                return jArr;
            }
            @Override
            protected void thenDoUiRelatedWork(JSONArray jArr) {
                ((MainActivity) mContext).spinnerViewFromObject(jArr);
            }
        });
    }

    public void dbSelectedSpinner(String selected_sname) {
        Needle.onBackgroundThread().execute(new UiRelatedTask<JSONObject>() {
            @Override
            protected JSONObject doWork() {
                String str;
                int iweight_min, iweight_max, num_min, num_max, ibox_min, ibox_max;
                int from_weight, to_weight, from_box, to_box, from_num, to_num;
                from_box = 100; to_box = 15000;

                JSONObject jobj = ((MainActivity) mContext).scaleRef.REFDB_selectFromAllItemsBysname(selected_sname);
                //Log.d(TAG,"#@# @@@@@@@@@@ dbSelectedSpinner(0) jobj ->"+jobj.toString());
                try {
                    ibox_min = jobj.getInt("ibox_min");
                    ibox_max = jobj.getInt("ibox_max");
                    ibox_min=(ibox_min>ibox_max)? ibox_max :ibox_min;
                    ibox_min = (ibox_min < from_box) ? from_box : ibox_min;
                    ibox_max = (ibox_max > to_box) ? to_box : ibox_max;
                    jobj.put("ibox_min",ibox_min);
                    jobj.put("ibox_max",ibox_max);
                    ////num
                    from_num = 1; to_num = 6;
                    num_min =  jobj.getInt("icombi_min");
                    num_max =  jobj.getInt("icombi_max");
                    num_min=(num_min>num_max) ? num_max :num_min;
                    num_min = (num_min < from_num) ? from_num : num_min;
                    num_max = (num_max > to_num) ? to_num : num_max;
                    jobj.put("icombi_min",num_min);
                    jobj.put("icombi_max",num_max);
                    //
                    from_weight=10;
                    to_weight = 3000;
                    iweight_min = jobj.getInt("iweight_min");
                    iweight_max = jobj.getInt("iweight_max");
                    iweight_min=(iweight_min>iweight_max) ? iweight_max :iweight_min;
                    iweight_min = (iweight_min < from_weight) ? from_weight : iweight_min;
                    iweight_max = (iweight_max > to_weight) ? to_weight : iweight_max;
                    jobj.put("iweight_min",iweight_min);
                    jobj.put("iweight_max",iweight_max);

                } catch (JSONException e) {
                }

                return jobj;
            }

            @Override
            protected void thenDoUiRelatedWork(JSONObject jobj) {
                float icombi_low, icombi_high, iweight_low, iweight_high, ibox_low, ibox_high;
                float from_weight, to_weight, from_box, to_box, from_num, to_num;
                Log.d(TAG,"#@# @@@@@@@@@@ dbSelectedSpinner(1) jobj ->"+jobj.toString());
                try {
                    ((MainActivity) mContext).textView_selected_title.setText(jobj.getString("sname"));
                    from_box = 0.1f;
                    to_box = 15.0f;//((icombi_max+1)*iweight_max)/1000;
                    ibox_low = (float) jobj.getInt("ibox_min")/1000.0f;
                    ibox_high = (float) jobj.getInt("ibox_max")/1000.0f;
                    ((MainActivity) mContext).sliderView_box_weight.setValueFrom(from_box);
                    ((MainActivity) mContext).sliderView_box_weight.setValueTo(to_box);
                    ((MainActivity) mContext).sliderView_box_weight.setValues(ibox_low, ibox_high);

                    //num
                    from_num = 1.0f; to_num = 6.0f;
                    icombi_low = (float) jobj.getInt("icombi_min");
                    icombi_high = (float) jobj.getInt("icombi_max");
                    ((MainActivity) mContext).sliderView_combin_num.setValueFrom(from_num);
                    ((MainActivity) mContext).sliderView_combin_num.setValueTo(to_num);
                    ((MainActivity) mContext).sliderView_combin_num.setValues(icombi_low, icombi_high);
                    //String str_combiNum = String.format("%d - %d 개", Math.round(icombi_min), Math.round(icombi_max));
                    //((MainActivity) mContext).textView_comb_num.setText(str_combiNum);

                    //개당무게
                    from_weight=10.0f;
                    to_weight = 3000.0f;
                    iweight_low = (float) (jobj.getInt("iweight_min")/10)*10;
                    iweight_high = (float)(jobj.getInt("iweight_max")/10)*10;
                    ((MainActivity) mContext).sliderView_combi_weight.setValueFrom(from_weight);
                    ((MainActivity) mContext).sliderView_combi_weight.setValueTo(to_weight);
                    ((MainActivity) mContext).sliderView_combi_weight.setValues(iweight_low, iweight_high);

                } catch (JSONException e) {
                }
            }
        });
    }
}
