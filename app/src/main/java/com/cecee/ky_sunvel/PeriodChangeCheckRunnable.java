package com.cecee.ky_sunvel;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import needle.Needle;
import needle.UiRelatedTask;

public class PeriodChangeCheckRunnable implements Runnable {
    private static final String TAG = "#@#";
    private final int WAITmillisecond = 300;
    private final int Testmillisecond = 1000;
    Context mContext;
    private boolean stopped = false;
    private int waitMillisec;
    private int loopCnt = 0;
    private int tare_cnt = 0;
    PeriodChangeCheckRunnable(Context context) {
        mContext = context;
    }
    int[] exScale_data=new int[16];

    @Override
    public void run() {
        while (!stopped) {
            //Log.d(TAG,"#@# ======== loop ========");
            loopCnt++;

            waitMillisec = (((MainActivity) mContext).TEST) ? Testmillisecond: WAITmillisecond;
            //date view update
            if(loopCnt>30){
                loopCnt=0;
                ((MainActivity) mContext).currentStatus.fun_CurrentDateTimeFromSystemTime();
                ((MainActivity) mContext).currentStatus.funSetCurrent_date_timeViewUI();
            }
            int[] mScale = ((MainActivity) mContext).jni.jniGetScale();
            if(!((MainActivity) mContext).TEST){
                ((MainActivity) mContext).jni.jniSetLed(((MainActivity) mContext).currentStatus.getItemViewColor());
            }
            if(mScale[16]>0 || (mScale[0]==0 && mScale[1]==0)) {
                try {
                    Thread.sleep(200);
                } catch (Exception e) {e.printStackTrace();}
                 continue;
            }//error이면

            int[] mDataset = scale_raw2view(mScale);
            ((MainActivity) mContext).currentStatus.setScale_raw(mScale);
            ((MainActivity) mContext).currentStatus.setScale_data(mDataset);
            ((MainActivity) mContext).currentStatus.fun_CurrentDateTimeFromSystemTime();

            //int[] tmp= ((MainActivity) mContext).currentStatus.getLocal_tare();
            //Log.d(TAG,"#@# @@@@@@@@@@(x) db getLocal_tare ->"+ Arrays.toString(tmp));
            if(((MainActivity) mContext).RE_COMBINATION){
                Arrays.fill(mDataset,0);
                changing_process(mDataset);
                ((MainActivity) mContext).RE_COMBINATION=false;
            }
            else {
                if (change_detection(mDataset, exScale_data)) {
                    Log.d(TAG, "#@# ======== changed!!! ========");
                    Log.d(TAG, "#@# @@@@@@@@@@(x) __Scale_data ->" + Arrays.toString(mDataset));
                    Log.d(TAG, "#@# @@@@@@@@@@(x) exScale_data ->" + Arrays.toString(exScale_data));
                    Log.d(TAG, "#@# ======== changed!!! ========\n");
                    //((MainActivity) mContext).currentStatus.combi_cnt=0;
                    changing_process(mDataset);
                }
            }
            if(((MainActivity) mContext).TEST){
                int[] led_color=new int[16];
                ((MainActivity) mContext).loop_cnt %=5;
                Arrays.fill(led_color, ((MainActivity) mContext).loop_cnt++);
                ((MainActivity) mContext).jni.jniSetLed(led_color);
            }
           // else ((MainActivity) mContext).jni.jniSetLed(((MainActivity) mContext).currentStatus.getItemViewColor());
            exScale_data=mDataset;
            try {
                Thread.sleep(waitMillisec);
            } catch (Exception e) {e.printStackTrace();}
            //Log.d(TAG, "#@# PeriodChangeCheckRunnable~~~");
        }
    }

    public boolean change_detection(int[] current, int[] before) {
        boolean changed=false;
        for(int i=0;i<14;i++){
            if(Math.abs(current[i]-before[i])>2){
                changed=true;
                break;
            }
        }
        return changed;
    }

    public void changing_process(int[] mDataset) {
        Needle.onBackgroundThread().execute(new UiRelatedTask<JSONObject>() {
            @Override
            protected JSONObject doWork() {
                JSONObject jobj=((MainActivity) mContext).currentStatus.make_combination_object(mDataset);
                return jobj;
            }
            @Override
            protected void thenDoUiRelatedWork(JSONObject jobj) {
                Log.d(TAG,"#@# @@@@@@@@@@(AAA) jobj ->"+ jobj.toString());
                 if (((MainActivity) mContext).menu_position == 0) {
                    if (!((MainActivity) mContext).TEST) {
                        make_combination_view(jobj,mDataset);
                    }
                    //색상결정
                    //Value 결정
                    if(!jobj.isNull("take_in")) ((MainActivity) mContext).recyclerManager.mAdapter_scale.notifyDataSetChanged();
                }
            }
        });
    }


    public void make_combination_view(JSONObject result, int[] value) {
        //JSONObject result=current_container_obj.;
        Log.d(TAG,"#@# @@@@@@@@@@(0) mainNewJarry["+ result.length()+"] ->"+result.toString());
        JSONArray takeOutJarry= new JSONArray();
        JSONArray takeInJarry= new JSONArray();
        try {
            takeOutJarry = result.getJSONArray("take_out");
            takeInJarry = result.getJSONArray("take_in");
            set_combination_textView(takeInJarry);
            set_takeOut_textView(takeOutJarry);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ((MainActivity) mContext).currentStatus.setEx_jArry_combi(takeInJarry);
    }

    void set_takeOut_textView (JSONArray jarr) {
        int jarrCnt=jarr.length();
        //Log.d(TAG, "#@# @@@@@@@@@@ set_complete_textView()->" + jarr.toString());
        if(0==jarrCnt) {
            return;
        }
        for (int i = 0; i < jarrCnt; i++) {
            try {
                JSONObject mjobj = jarr.getJSONObject(i);
                JSONArray mjarrIdx = mjobj.getJSONArray("idx");
                JSONArray mjarrVal = mjobj.getJSONArray("value");
                int combi_idx= mjobj.getInt("combi_idx");
                Log.d(TAG, "#@# @@@@@@@ set_takeOut_textView mjobj.toString()->" + mjobj.toString());
                ((MainActivity) mContext).currentStatus.system_total_box_num++;
                ((MainActivity) mContext).currentStatus.system_total_weight=((MainActivity) mContext).currentStatus.system_total_weight+mjobj.getInt("weight");
                String str= ((MainActivity) mContext).Formatter_3digit.format(((MainActivity) mContext).currentStatus.system_total_box_num);
                str = String.format("%s 개", str);
                ((MainActivity) mContext).textView_system_box_num.setText(str);
                str = String.format("%.2f Kg", ((MainActivity) mContext).currentStatus.system_total_weight/1000.00f);
                ((MainActivity) mContext).textView_system_total_weight.setText(str);
                str = String.format("%.2f g", (float)((MainActivity) mContext).currentStatus.system_total_weight/((MainActivity) mContext).currentStatus.system_total_box_num);
                ((MainActivity) mContext).textView_system_box_average_weight.setText(str);
                ((MainActivity) mContext).db_manager.dbUpdate_today_product(mjobj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    void set_combination_textView(JSONArray jarr) {
        int jarrCnt=jarr.length();
        //int[] check={0,0,0,0};
        int[] items_color=new int[14];
        int[] items_weights=new int[4];
        Arrays.fill(items_color, 0);
        Arrays.fill(items_weights, 0);

        int[] Occupy_color =((MainActivity) mContext).currentStatus.getOccupy_color();//[tmp] = 0x00;
        Arrays.fill(Occupy_color, 0);
        for (int i = 0; i < jarrCnt; i++) {
            try {
                JSONObject mjobj = jarr.getJSONObject(i);
                JSONArray mjarrIdx = mjobj.getJSONArray("idx");
                JSONArray mjarrVal = mjobj.getJSONArray("value");
                int weight= mjobj.getInt("weight");
                int combi_idx= mjobj.getInt("combi_idx");
                Occupy_color[combi_idx]=0xAA;
                if(combi_idx<4) items_weights[combi_idx]=weight;
                //check[combi_idx]=1;
                //viewChangeIdx(combi_idx,weight);
                int combi_cnt = mjarrIdx.length();
                for (int ix = 0; ix < combi_cnt; ix++) {
                    int _idx = mjarrIdx.getInt(ix);
                    items_color[_idx] = combi_idx+1;
                 }
                Log.d(TAG, "#@# @@@@@@@@@@ mjobj.toString()->" + mjobj.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        ((MainActivity) mContext).currentStatus.setOccupy_color(Occupy_color);
        viewChangeIdx(items_weights);
        ((MainActivity) mContext).currentStatus.setItemViewColor(items_color);
    }

    public void viewChangeIdx (int[] weights) {
        for(int i=0; i<4;i++) {
            String formattedStringNum = ((MainActivity) mContext).Formatter_3digit.format(weights[i]);
            String str = String.format("%s g", formattedStringNum);
            switch (i) {
                case 0:
                    ((MainActivity) mContext).textView_combi_sum1.setText(str);
                    break;
                case 1:
                    ((MainActivity) mContext).textView_combi_sum2.setText(str);
                    break;
                case 2:
                    ((MainActivity) mContext).textView_combi_sum3.setText(str);
                    break;
                case 3:
                    ((MainActivity) mContext).textView_combi_sum4.setText(str);
                    break;
            }
        }
    }


    int[] scale_raw2view(int[] raw) {
        int[] m_vraw = new int[14];
        int[] m_tare = ((MainActivity) mContext).currentStatus.getLocal_tare();
        float[] m_factor = ((MainActivity) mContext).currentStatus.getLocal_f1k();

        if(((MainActivity) mContext).TEST){
            for (int i = 0; i < 14; i++) m_vraw[i] =raw[i];
        }
        else {
            for (int i = 0; i < 14; i++) {
                float factor = m_factor[i];
                int real_scale = (int) ((raw[i] * factor) - (m_tare[i] * factor));
                real_scale=(real_scale<5) ? 0:real_scale;//scale view값이 마이너스로 나오면 0로
                m_vraw[i]=real_scale;
            }
        }
        //Log.d(TAG,"getLocal_raw    ~~raw      "+ Arrays.toString(raw));
        //Log.d(TAG,"getLocal_tare  ~~~itare    "+ Arrays.toString(m_tare));
        //Log.d(TAG,"getLocal_factor~~~m_factor "+ Arrays.toString(m_factor));
        //Log.d(TAG,"getLocal_factor~~~m_vraw   "+ Arrays.toString(m_vraw));
        return m_vraw;
    }

}
