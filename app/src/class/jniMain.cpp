#include "com_cecee_ky_sunvel_JniCall.h"

//#include "extern.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <android/log.h>
#include <string.h>
//#include <pthread.h>
#include <string>
#include <locale.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <sys/ioctl.h>
#include <sys/poll.h>
#include <termios.h>
#include <zconf.h>

#if 0
//#include <zip.h>
//#include "alsaplayer.h"
//#include <memory>
//#include <fstream>
//#include <iostream>
#endif
#define  LOG_TAG "cecee"
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
#define  TRACE(...) __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
using namespace std;

int UART_0 = -1;
int UART_1 = -1;
int UART_3 = -1;

//JNIEnv *env;
extern "C" {
void openUart(int uart) {
    struct termios newtio;
    int TTY;
    char tty[32] = {0,};

    switch (uart) {
        case 0:
            sprintf(tty, "/dev/ttyS0");
            TTY = UART_0;
            break;
        case 1:
            sprintf(tty, "/dev/ttyS1");
            TTY = UART_1;
            break;
        case 3:
            sprintf(tty, "/dev/ttyS3");
            TTY = UART_3;
            break;
    }
    if (TTY < 0) {
        TTY = open(tty, O_RDWR | O_NOCTTY | O_NONBLOCK);
        // fd=open("/dev/ttyS3", O_RDWR|O_NDELAY|O_NOCTTY);
    } else {
        close(TTY);
        TTY = -1;
        TTY = open(tty, O_RDWR | O_NOCTTY | O_NONBLOCK);
    }

    TRACE("#@#MIDIIO_OpenSerial ------uart[%d]  fd[%d]\n", uart, TTY);

    bzero(&newtio, sizeof(newtio)); /* clear struct for new port settings */
    tcgetattr(TTY, &newtio); /* save current serial port settings */
    newtio.c_cflag &= ~PARENB;        // Make 8n1
    newtio.c_cflag &= ~CSTOPB;
    newtio.c_cflag &= ~CSIZE;
    newtio.c_cflag |= CS8;
    newtio.c_cflag &= ~CRTSCTS;       // no flow control
    newtio.c_lflag = 0;          // no signaling chars, no echo, no canonical processing
    newtio.c_oflag = 0;                  // no remapping, no delays
    newtio.c_cc[VMIN] = 128;                  // read doesn't block
    newtio.c_cc[VTIME] = 0;                  // 0.5 seconds read timeout

    newtio.c_cflag |= CREAD | CLOCAL;     // turn on READ & ignore ctrl lines
    newtio.c_iflag &= ~(IXON | IXOFF | IXANY);// turn off s/w flow ctrl
    newtio.c_iflag &= ~(INLCR | ICRNL);//0x0D-->0x0A로 변환되어 올라오는문제

    newtio.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG); // make raw
    newtio.c_oflag &= ~OPOST;              // make raw

    //newtio.c_iflag = IGNCR;

    tcflush(TTY, TCIFLUSH); //TCIOFLUSH
    tcsetattr(TTY, TCSANOW, &newtio);

    switch (uart) {
        case 0:
            cfsetospeed(&newtio, B115200);
            UART_0 = TTY;
            break;
        case 1:
            //  cfsetospeed(&newtio,B31250);
            UART_1 = TTY;
            break;
        case 3:
            //  cfsetospeed(&newtio,B31250);
            UART_3 = TTY;
            break;
    }
}

void openSerial() {
    openUart(0);
    openUart(1);
    openUart(3);
}


JNIEXPORT void JNICALL Java_com_cecee_ky_1sunvel_JniCall_jniOpenSerial(JNIEnv *env, jobject obj) {
    TRACE("#@# jniOpenSerial\n");
    openSerial();
}

uint8_t calcu_check_sum(const uint8_t* data, uint8_t len){
    uint8_t checksum = 0;
    for (int ix = 0; ix < len; ix++) {
         checksum = checksum +data[ix];
        //TRACE("#@@ [%d]  data[%x]\n",ix, data[ix]);
    }
    return checksum;
}

JNIEXPORT jintArray JNICALL
Java_com_cecee_ky_1sunvel_JniCall_jniGetScale(JNIEnv *env, jobject obj) {
    //저울읽기 command ? AA 55 @
    unsigned char cmd_buf[4] = {'>', '?', 'A', '@'};//start, idx, RR, GG, BB
    unsigned char rx_buf[256] = {0,};
    static int scale_raw[32] = {0,};
    int tmpArr[32];
    int i, ix, err;
    int res;
    int checksum = 0;
    //for (i = 0; i < 3; i++) {

        read(UART_3, rx_buf, sizeof(rx_buf));//flush
        memset(rx_buf,0,sizeof(rx_buf));
        write(UART_3, cmd_buf, 4);//port test
        usleep(50000);//31.25k 1sec 3k 100 300 50ms 150byte

        read(UART_3, rx_buf, sizeof(rx_buf));
        err = 0;
        if (rx_buf[0] == '<' && rx_buf[65] == '@') {
            checksum= calcu_check_sum(rx_buf,65);
             if (checksum != rx_buf[66]) {
                TRACE("@#@@ Err  checksum[%x] = [%x]\n",checksum, rx_buf[66]);
                err = 1;
            }
        }
        if (err) {
            TRACE("@#@@ Err  checksum[%x] = [%x]\n",checksum, rx_buf[66]);
            scale_raw[16] = 0x01;//error
        }
        else {
            //TRACE("@#@@ OK  checksum[%x] = [%x]\n",checksum, rx_buf[66]);
            scale_raw[16] = 0x00;//no error
            memcpy(&scale_raw[0], rx_buf + 1, 64);//header 제외하고
        }
   // }

    memcpy(&scale_raw[0], rx_buf + 1, 64);//header 제외하고

    jintArray retArr = env->NewIntArray(18);
    for (int i = 0; i < 18; i++) tmpArr[i] = scale_raw[i];
    env->SetIntArrayRegion(retArr, 0, 16, tmpArr);
    return retArr;
}

JNIEXPORT void JNICALL
Java_com_cecee_ky_1sunvel_JniCall_jniSetLed(JNIEnv *env, jobject obj, jintArray led_data) {
    unsigned char led[32] = {
            0,};//{'>','!', 'L',0x02,0x03,0x04,.... 0xff, 0xff, 0x80,'@'};//start, idx, 16개 LED color값 내려줌
    jint *intBuf;
    int size = env->GetArrayLength(led_data);
    intBuf = env->GetIntArrayElements(led_data, NULL);
    for (int i = 0; i < size; i++) {
        led[i + 3] = intBuf[i];
    }
    led[0] = '>';
    led[1] = '!';
    led[2] = 'L';
    led[19] = '@';
    write(UART_3, led, 20);//port test
    usleep(100);
    env->ReleaseIntArrayElements(led_data, intBuf, 0);
}


}